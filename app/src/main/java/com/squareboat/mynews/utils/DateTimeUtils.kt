package com.squareboat.mynews.utils

import android.text.format.DateUtils
import com.squareboat.mynews.BuildConfig
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

fun getTTimesAgo(dateStr: String): String {
    val inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault())
    var date: Date? = null
    var newDate = ""
    try {
        date = inputFormat.parse(dateStr)
    } catch (e: ParseException) {
        e.printStackTrace()
    }
    if (BuildConfig.DEBUG && date == null) {
        error("Assertion failed")
    }
    if (date != null) {
        newDate = DateUtils.getRelativeTimeSpanString(
            date.time,
            Calendar.getInstance().timeInMillis,
            DateUtils.MINUTE_IN_MILLIS
        ) as String
    }
    return newDate
}

fun getPreetyDate(datetime: String): String {
    val year = datetime.substring(0, 4)
    val month = datetime.substring(5, 7)
    val cal = Calendar.getInstance()
    val monthDate = SimpleDateFormat("MMMM", Locale.getDefault())
    val monthNum = month.toInt()
    cal[Calendar.MONTH] = monthNum
    val monthName = monthDate.format(cal.time)
    val date = datetime.substring(8, 10)
    val time = convertToTwelveHourFormat(datetime.substring(11, 16))

    return "$year $monthName $date at $time"
}

fun convertToTwelveHourFormat(time: String): String {
    try {
        val sdf = SimpleDateFormat("H:mm", Locale.getDefault())
        val dateObj = sdf.parse(time)
        return SimpleDateFormat("K:mm a", Locale.getDefault()).format(dateObj)
    } catch (e: ParseException) {
        e.printStackTrace()
    }
    return ""
}