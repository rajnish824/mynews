package com.squareboat.mynews.utils.extensions

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.browser.customtabs.CustomTabsIntent
import com.squareboat.mynews.ui.newsDetails.NewsDetailsActivity

fun browserIntent(url: String, context: Context){
    val builder = CustomTabsIntent.Builder()
    val customTabsIntent = builder.build()
    customTabsIntent.launchUrl(context, Uri.parse(url))
}
fun dataIntent(context: Context, dataMap: HashMap<String, String>){
    val intent = Intent(context, NewsDetailsActivity::class.java)
    intent.putExtra("data", dataMap)
    context.startActivity(intent)
}