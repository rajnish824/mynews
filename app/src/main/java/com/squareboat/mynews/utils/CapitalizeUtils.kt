package com.squareboat.mynews.utils

import java.util.*

fun stringCapitalize(text: String): String {
    val words = text.split(" ").toMutableList()
    var output = ""
    for (word in words) {
        output += word.capitalize(Locale.ROOT) + " "
    }
    return output.trim()
}