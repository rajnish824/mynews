package com.squareboat.mynews.utils

import android.content.Context
import android.content.SharedPreferences

fun saveProvidedLocation(context: Context, locationCode: String, locationName: String) {
    val sharedLocation: SharedPreferences = context.getSharedPreferences(
        AppConst.locationPref,
        Context.MODE_PRIVATE
    )
    val editor: SharedPreferences.Editor = sharedLocation.edit()
    editor.putString(AppConst.locCode, locationCode)
    editor.putString(AppConst.locName, locationName)
    editor.apply()
    editor.commit()
}