package com.squareboat.mynews.utils

object AppConst {
    val sortBy = arrayOf("publishedAt", "popularity", "relevancy")

    val category = arrayOf("Business", "Entertainment", "General", "Health", "Science", "Sports", "Technology")

    const val initialPageSize = 10
    const val incrementSize = 10

    const val apiKey = "6f403a4f24fe4cc68b42ba472a44a396"

    const val locationPref = "sharedLocation"
    const val locCode = "locationCode"
    const val locName = "locationName"

    const val image = "image"
    const val title = "title"
    const val description = "description"
    const val time = "time"
    const val source = "source"
    const val url = "url"


}