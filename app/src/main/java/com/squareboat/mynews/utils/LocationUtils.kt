package com.squareboat.mynews.utils

import java.util.*
import kotlin.collections.HashMap

fun currentLocation(): HashMap<String, String> {
    val hashMap: HashMap<String, String> = HashMap()
    hashMap["code"] = Locale.getDefault().country.toLowerCase()
    hashMap["name"] = stringCapitalize(Locale.getDefault().displayCountry)
    return hashMap
}

fun getCountryNames(): HashMap<String, String> {
    val countryMap: HashMap<String, String> = HashMap()
    countryMap["it"] = "Italy"
    countryMap["in"] = "India"
    countryMap["ve"] = "Venezuela"
    countryMap["jp"] = "Japan"
    countryMap["au"] = "Australia"
    countryMap["mx"] = "Mexico"
    countryMap["tr"] = "Turkey"
    countryMap["gb"] = "United Kingdom"
    return countryMap
}

