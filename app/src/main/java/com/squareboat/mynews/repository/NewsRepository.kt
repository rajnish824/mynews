package com.squareboat.mynews.repository

import com.squareboat.mynews.network.APIService

class NewsRepository(private val apiService: APIService) {
    suspend fun getHeadlines(
        country: String,
        category: String,
        sources: String,
        q: String,
        pageSize: Int,
        page: Int
    ) = apiService.getHeadLines(country, category, sources, q, pageSize, page)

    suspend fun getAllNews(
        q: String,
        qInTitle: String,
        sources: String,
        from: String,
        to: String,
        language: String,
        sortBy: String,
        pageSize: Int,
        page: Int
    ) = apiService.getEverything(q, qInTitle, sources, from, to, language, sortBy, pageSize, page)

    suspend fun getAllSources(category: String, language: String, country: String) =
        apiService.getSources(category, language, country)
}