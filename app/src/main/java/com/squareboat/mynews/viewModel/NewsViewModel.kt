package com.squareboat.mynews.viewModel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.squareboat.mynews.model.ArticleResponse
import com.squareboat.mynews.model.DataWrapper
import com.squareboat.mynews.model.SourceResponse
import com.squareboat.mynews.repository.NewsRepository
import kotlinx.coroutines.launch
import org.koin.standalone.KoinComponent

class NewsViewModel(private val newsRepository: NewsRepository) : ViewModel(),
    KoinComponent {
    private var headlineMutableList = MutableLiveData<DataWrapper<List<ArticleResponse.Article>>>()
    var allHeadline: LiveData<DataWrapper<List<ArticleResponse.Article>>> = headlineMutableList
    fun getHeadlines(
        country: String = "in",
        category: String = "",
        sources: String = "",
        q: String = "",
        pageSize: Int = 10,
        page: Int = 1
    ) {
        viewModelScope.launch {
            try {
                val response =
                    newsRepository.getHeadlines(country, category, sources, q, pageSize, page)
                headlineMutableList.postValue(DataWrapper(response = response.articles))
            } catch (e: Exception) {
                headlineMutableList.postValue(DataWrapper(exception = e))
            }
        }
    }

    private var allNewsMutableList = MutableLiveData<DataWrapper<List<ArticleResponse.Article>>>()
    var allNewsList: LiveData<DataWrapper<List<ArticleResponse.Article>>> = allNewsMutableList

    fun getAllNews(
        q: String = "",
        qInTitle: String = "",
        sources: String = "",
        from: String = "",
        to: String = "",
        language: String = "",
        sortBy: String = "",
        pageSize: Int = 10,
        page: Int = 1
    ) {
        viewModelScope.launch {
            try {
                val response = newsRepository.getAllNews(
                    q,
                    qInTitle,
                    sources,
                    from,
                    to,
                    language,
                    sortBy,
                    pageSize,
                    page
                )
                allNewsMutableList.postValue(DataWrapper(response = response.articles))
            } catch (e: Exception) {
                Log.e("Error in Everything:", e.stackTraceToString())
                allNewsMutableList.postValue(DataWrapper(exception = e))
            }
        }
    }

    private var listOfSources = MutableLiveData<List<SourceResponse.Source>>()
    val allSources: LiveData<List<SourceResponse.Source>> = listOfSources

    fun getAllSources(category: String = "", language: String = "", country: String = "") {
        viewModelScope.launch {
            try {
                val response = newsRepository.getAllSources(category, language, country)
                listOfSources.postValue(response.sources)
            } catch (e: Exception) {

            }
        }
    }
}