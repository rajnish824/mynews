package com.squareboat.mynews.network

import com.squareboat.mynews.model.ArticleResponse
import com.squareboat.mynews.model.SourceResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface APIService {
    @GET("/v2/top-headlines")
    suspend fun getHeadLines(
        @Query("country") country: String,
        @Query("category") category: String,
        @Query("sources") sources: String,
        @Query("q") q: String,
        @Query("pageSize") pageSize: Int,
        @Query("page") page: Int
    ): ArticleResponse

    @GET("/v2/everything")
    suspend fun getEverything(
        @Query("q") q: String,
        @Query("qInTitle") qInTitle: String,
        @Query("sources") sources: String,
        @Query("from") from: String,
        @Query("to") to: String,
        @Query("language") language: String,
        @Query("sortBy") sortBy: String,
        @Query("pageSize") pageSize: Int,
        @Query("page") page: Int
    ): ArticleResponse

    @GET("/v2/sources")
    suspend fun getSources(
        @Query("category") category: String,
        @Query("language") language: String,
        @Query("country") country: String
    ): SourceResponse
}