package com.squareboat.mynews.network

import com.squareboat.mynews.utils.AppConst
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

private fun basicOkHttpClient() = OkHttpClient.Builder().apply {
    addInterceptor(Interceptor { chain ->
        val builder = chain.request().newBuilder()
            .addHeader(
                "Authorization", AppConst.apiKey
            )
        return@Interceptor chain.proceed((builder.build()))
    })
}.build()


fun createArticleService(): APIService {
    val baseUrl = "https://newsapi.org"
    val retrofit = Retrofit.Builder()
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create()).client(basicOkHttpClient())
        .baseUrl(baseUrl)
        .build()
    return retrofit.create(APIService::class.java)
}