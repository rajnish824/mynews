package com.squareboat.mynews.base

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.squareboat.mynews.R
import com.squareboat.mynews.ui.error.ErrorActivity

abstract class BaseActivity : AppCompatActivity(), BaseErrorInterface {

    protected open fun init() {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        init()
    }

    override fun onBadRequest(message: String) {
        super.onBadRequest(message)
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun onTooManyRequest(message: String) {
        super.onTooManyRequest(message)
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun onUnAuthorized(message: String) {
        super.onUnAuthorized(message)
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun showOtherError(message: String) {
        super.showOtherError(message)
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun showInternetError() {
        val intent = Intent(this, ErrorActivity::class.java)
        startActivity(intent)
    }

    override fun showSocketTimeOutException() {
        Toast.makeText(this, getString(R.string.socket_timeout), Toast.LENGTH_SHORT).show()
    }

}