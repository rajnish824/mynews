package com.squareboat.mynews.base

import com.google.gson.Gson
import com.squareboat.mynews.model.ErrorResponse
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException

interface BaseErrorInterface {
    fun showError(throwable: Throwable) {
        when (throwable) {
            is HttpException -> {
                val response = throwable.response()

                val errorResponse =
                    Gson().fromJson(response?.errorBody()!!.toString(), ErrorResponse::class.java)
                when (response.code()){
                    400 -> onBadRequest(errorResponse.message)
                    401 -> onUnAuthorized(errorResponse.message)
                    429 -> onTooManyRequest(errorResponse.message)
                    else -> showOtherError(errorResponse.message)
                }
            }
            is SocketTimeoutException -> showSocketTimeOutException()
            is IOException -> showInternetError()
        } 
    }

    fun onBadRequest(message: String) {

    }

    fun onUnAuthorized(message: String) {

    }

    fun onTooManyRequest(message: String) {

    }

    fun showOtherError(message: String) {

    }

    fun showSocketTimeOutException()

    fun showInternetError()



}