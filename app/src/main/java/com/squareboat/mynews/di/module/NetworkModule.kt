package com.squareboat.mynews.di.module

import com.squareboat.mynews.network.createArticleService
import org.koin.dsl.module.module

val networkModule = module {
    single { createArticleService() }
}