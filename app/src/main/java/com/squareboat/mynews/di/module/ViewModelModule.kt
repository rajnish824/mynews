package com.squareboat.mynews.di.module

import com.squareboat.mynews.viewModel.NewsViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val viewModelModule = module {
    viewModel { NewsViewModel(get()) }
}