package com.squareboat.mynews.di.module

import com.squareboat.mynews.repository.NewsRepository
import org.koin.dsl.module.module

val repositoryModule = module {
    single { NewsRepository(get()) }
}