package com.squareboat.mynews.ui.category

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Adapter
import android.widget.ArrayAdapter
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.squareboat.mynews.databinding.ItemCategorySheetBinding
import com.squareboat.mynews.utils.AppConst

class CategoryBottomSheet : BottomSheetDialogFragment() {
    private var categorySheetListener: CategorySheetListener? = null
    private lateinit var adapter: Adapter
    private lateinit var categorySheetBinding: ItemCategorySheetBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = ItemCategorySheetBinding.inflate(inflater, container, false)
        categorySheetBinding = binding
        adapter =
            ArrayAdapter(
                requireContext(),
                android.R.layout.simple_list_item_1, AppConst.category
            )
        binding.category.adapter = adapter as ArrayAdapter<*>
        binding.category.setOnItemClickListener { _, _, position, _ ->
            categorySheetListener!!.onCategoryClick(position)
            dismiss()
        }
        return binding.root
    }

    interface CategorySheetListener {
        fun onCategoryClick(position: Int)
    }

    fun onItemCategoryListener(categorySheetListener: CategorySheetListener) {
        this.categorySheetListener = categorySheetListener
    }
}


