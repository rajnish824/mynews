package com.squareboat.mynews.ui.search

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.squareboat.mynews.R
import com.squareboat.mynews.base.BaseActivity
import com.squareboat.mynews.databinding.ActivitySearchBinding
import com.squareboat.mynews.ui.dashboard.NewsAdapter
import com.squareboat.mynews.ui.filter.FilterBottomSheet
import com.squareboat.mynews.ui.location.LocationBottomSheet
import com.squareboat.mynews.utils.AppConst
import com.squareboat.mynews.utils.currentLocation
import com.squareboat.mynews.utils.extensions.hideKeyboard
import com.squareboat.mynews.utils.extensions.showKeyBoard
import com.squareboat.mynews.utils.saveProvidedLocation
import com.squareboat.mynews.viewModel.NewsViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class SearchActivity : BaseActivity(),
    FilterBottomSheet.FilterSheetListener, AdapterView.OnItemSelectedListener,
    LocationBottomSheet.LocationSheetListener {

    private val newsViewModel: NewsViewModel by viewModel()

    private lateinit var newsAdapter: NewsAdapter
    private var recentlySearched: String = ""
    private var recentlySorted: String = ""
    private var recentSource: String = ""
    private var currentPageSize: Int = AppConst.initialPageSize

    private lateinit var binding: ActivitySearchBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySearchBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        //toolbar
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        binding.searchNews.requestFocus()
        binding.searchNews.showKeyBoard()

        binding.loadMore.hide()
        binding.filter.shrink()
        binding.loadMore.shrink()

        //init
        initRecyclerView()
        initSearch()
        setSortBy()
        setUserLocation()
        initPagination()

        getAllNews("corona", recentlySorted, recentSource, currentPageSize)

        binding.filter.setOnClickListener {
            val filterSheet = FilterBottomSheet()
            filterSheet.onItemFilterListener(this)
            filterSheet.show(supportFragmentManager, "FilterSheet")
        }
        binding.locationLayout.setOnClickListener {
            val bottomSheet = LocationBottomSheet()
            bottomSheet.onItemLocationListener(this)
            bottomSheet.show(supportFragmentManager, "LocationSheet")
            binding.searchNews.hideKeyboard()
        }
        binding.loadMore.setOnClickListener {
            loadMore()
        }
    }

    private fun initRecyclerView() {
        val layoutManager = LinearLayoutManager(this)
        binding.recyclerView.layoutManager = layoutManager
    }

    private fun getAllNews(
        searchText: String = "corona",
        sortBy: String = "",
        source: String = "",
        pageSize: Int
    ) {
        binding.mainProgress.show()
        newsViewModel.allNewsList.observe(this, { allNewsList ->
            allNewsList?.let {
                if (allNewsList.response == null) {
                    allNewsList.exception?.let { it1 -> showError(it1) }
                } else {
                    newsAdapter = NewsAdapter(allNewsList.response)
                    binding.recyclerView.adapter = newsAdapter
                    newsAdapter.notifyItemInserted(pageSize)
                    binding.recyclerView.smoothScrollToPosition(pageSize - AppConst.incrementSize)
                }
                binding.mainProgress.hide()
            }
        })
        newsViewModel.getAllNews(
            q = searchText,
            sortBy = sortBy,
            sources = source,
            pageSize = pageSize
        )
        recentSource = source
        recentlySorted = sortBy
        recentlySearched = searchText
    }

    private fun initPagination() {
        binding.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (recyclerView.canScrollVertically(1)) {
                    binding.loadMore.hide()
                } else {
                    binding.loadMore.show()
                }
            }
        })
    }

    private fun loadMore() {
        currentPageSize += AppConst.incrementSize
        getAllNews(
            searchText = recentlySearched,
            sortBy = recentlySorted,
            source = recentSource,
            pageSize = currentPageSize
        )
    }

    private fun resetPageSize() {
        currentPageSize = AppConst.initialPageSize
    }

    //search
    private fun initSearch() {
        binding.searchBtn.setOnClickListener {
            val searchInput: String = binding.searchNews.text.toString().trim()
            if (searchInput.isEmpty()) {
                Toast.makeText(this, getString(R.string.empty_search_message), Toast.LENGTH_SHORT)
                    .show()
            } else {
                getAllNews(
                    searchText = searchInput,
                    sortBy = recentlySorted,
                    source = recentSource,
                    currentPageSize
                )
                binding.heading.text = searchInput
                recentlySearched = searchInput
                binding.mainProgress.hide()
                binding.searchNews.hideKeyboard()
            }
        }
    }

    //sort
    private fun setSortBy() {
        val sortByAdapter =
            ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, AppConst.sortBy)
        binding.sortNews.adapter = sortByAdapter
        binding.sortNews.onItemSelectedListener = (this)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        resetPageSize()
        getAllNews(
            searchText = recentlySearched,
            sortBy = AppConst.sortBy[position],
            source = recentSource,
            currentPageSize
        )
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    //source
    override fun onSourceClick(sourcesSelected: String) {
        resetPageSize()
        getAllNews(
            searchText = recentlySearched,
            sortBy = recentlySorted,
            source = sourcesSelected,
            currentPageSize
        )
    }

    //location
    override fun onOptionClick(locationName: String, locationCode: String) {
        saveProvidedLocation(this, locationCode, locationName)
        binding.location.text = locationName
    }


    private fun setUserLocation() {
        currentLocation()["code"]?.let {
            currentLocation()["name"]?.let { it1 ->
                saveProvidedLocation(
                    this,
                    it,
                    it1
                )
                binding.location.text = it1
            }
        }
    }

    //navigation up
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}