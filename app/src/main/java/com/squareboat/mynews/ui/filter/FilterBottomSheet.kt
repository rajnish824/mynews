package com.squareboat.mynews.ui.filter

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.SparseBooleanArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Adapter
import android.widget.ArrayAdapter
import android.widget.ListView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.squareboat.mynews.databinding.ItemFilterSheetBinding
import com.squareboat.mynews.utils.AppConst
import com.squareboat.mynews.viewModel.NewsViewModel
import org.koin.android.viewmodel.ext.android.viewModel


class FilterBottomSheet : BottomSheetDialogFragment() {
    private var filterSheetListener: FilterSheetListener? = null

    private val newsViewModel: NewsViewModel by viewModel()
    private lateinit var adapter: Adapter
    private lateinit var sharedLocation: SharedPreferences
    private lateinit var sourceMap: HashMap<String, String>
    private var itemsChecked: String = ""

    private lateinit var filterSheetBinding: ItemFilterSheetBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = ItemFilterSheetBinding.inflate(inflater, container, false)
        filterSheetBinding = binding
        getSources()

        binding.sources.setOnItemClickListener { _, _, _, _ ->
            val clickedItemPositions: SparseBooleanArray = binding.sources.checkedItemPositions

            for (index in 0 until clickedItemPositions.size()) {
                val checked = clickedItemPositions.valueAt(index)
                if (checked) {
                    val key = clickedItemPositions.keyAt(index)
                    val value = binding.sources.getItemAtPosition(key)
                    itemsChecked = "$value,$itemsChecked"
                }
            }
        }

        binding.apply.setOnClickListener {
            filterSheetListener!!.onSourceClick(itemsChecked)
            dismiss()
        }
        return binding.root
    }

    private fun getSources() {
        newsViewModel.allSources.observe(viewLifecycleOwner, { allsources ->
            allsources?.let { sourcesList ->
                sourcesList.let {
                    sourceMap = HashMap()
                    for (i in sourcesList.indices) {
                        sourceMap[sourcesList[i].id] = sourcesList[i].name
                    }
                    adapter =
                        ArrayAdapter(
                            requireContext(),
                            android.R.layout.simple_list_item_multiple_choice,
                            ArrayList<String>(sourceMap.keys)
                        )
                    filterSheetBinding.sources.choiceMode = ListView.CHOICE_MODE_MULTIPLE
                    filterSheetBinding.sources.adapter = adapter as ArrayAdapter<*>
                }
            }
        })

        sharedLocation = requireContext().getSharedPreferences(
            AppConst.locationPref,
            Context.MODE_PRIVATE
        )

        val locationCode = sharedLocation.getString(AppConst.locCode, "")
        locationCode?.let { newsViewModel.getAllSources(country = it) }
    }

    interface FilterSheetListener {
        fun onSourceClick(sourcesSelected: String)
    }

    fun onItemFilterListener(filterSheetListener: FilterSheetListener){
        this.filterSheetListener = filterSheetListener
    }
}