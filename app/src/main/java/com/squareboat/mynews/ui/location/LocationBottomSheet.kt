package com.squareboat.mynews.ui.location

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.squareboat.mynews.databinding.ItemLocationSheetBinding
import com.squareboat.mynews.utils.getCountryNames

class LocationBottomSheet : BottomSheetDialogFragment() {

    private var locationSheetListener: LocationSheetListener? = null
    private lateinit var locationSheetBinding: ItemLocationSheetBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = ItemLocationSheetBinding.inflate(inflater, container, false)
        locationSheetBinding = binding
        val countryList = ArrayList(getCountryNames().values)
        val adapter =
            ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, countryList)
        binding.locations.adapter = adapter
        binding.locations.setOnItemClickListener { parent, _, position, _ ->
            val location = parent.getItemAtPosition(position) as String
            val locationCode =
                getCountryNames().filterValues { it == location }.keys.toString().substring(1, 3)
            locationSheetListener!!.onOptionClick(location, locationCode)
            dismiss()
        }
        return binding.root
    }

    interface LocationSheetListener {
        fun onOptionClick(locationName: String, locationCode: String)
    }

    fun onItemLocationListener(locationSheetListener: LocationSheetListener) {
        this.locationSheetListener = locationSheetListener
    }
}