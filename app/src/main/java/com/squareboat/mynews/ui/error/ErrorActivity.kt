package com.squareboat.mynews.ui.error

import android.content.Intent
import android.os.Bundle
import com.squareboat.mynews.base.BaseActivity
import com.squareboat.mynews.databinding.ActivityErrorBinding
import com.squareboat.mynews.ui.dashboard.MainActivity

class ErrorActivity : BaseActivity() {

    private lateinit var binding: ActivityErrorBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityErrorBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        binding.retry.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}