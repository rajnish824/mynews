package com.squareboat.mynews.ui.newsDetails

import android.os.Bundle
import com.squareboat.mynews.base.BaseActivity
import com.squareboat.mynews.databinding.ActivityNewsDetailsBinding
import com.squareboat.mynews.utils.AppConst
import com.squareboat.mynews.utils.extensions.browserIntent
import com.squareboat.mynews.utils.extensions.setImage
import com.squareboat.mynews.utils.getPreetyDate

@Suppress("UNCHECKED_CAST")
class NewsDetailsActivity : BaseActivity() {

    private lateinit var binding: ActivityNewsDetailsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityNewsDetailsBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val intent = intent.extras
        val dataMap: HashMap<String, String> = intent?.get("data") as HashMap<String, String>

        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        binding.description.text = dataMap[AppConst.description].toString()
        binding.title.text = dataMap[AppConst.title].toString()
        binding.time.text = getPreetyDate(dataMap[AppConst.time].toString())
        binding.source.text = dataMap[AppConst.source].toString()
        binding.fullStory.setOnClickListener {
            browserIntent(dataMap[AppConst.url].toString(), this)
        }
        setImage(dataMap[AppConst.image].toString(), binding.progressCircular, binding.newsImage)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}