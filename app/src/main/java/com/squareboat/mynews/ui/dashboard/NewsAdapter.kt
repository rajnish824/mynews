package com.squareboat.mynews.ui.dashboard

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.squareboat.mynews.databinding.ItemNewsHolderBinding
import com.squareboat.mynews.model.ArticleResponse
import com.squareboat.mynews.utils.AppConst
import com.squareboat.mynews.utils.extensions.dataIntent
import com.squareboat.mynews.utils.getTTimesAgo

class NewsAdapter constructor(
    private val newsList: List<ArticleResponse.Article>
) :
    RecyclerView.Adapter<NewsAdapter.NewsViewHolder>() {

    private lateinit var itemNewsHolderBinding: ItemNewsHolderBinding
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsAdapter.NewsViewHolder {

        val binding =
            ItemNewsHolderBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        itemNewsHolderBinding = binding
        return NewsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: NewsAdapter.NewsViewHolder, position: Int) {
        Glide.with(holder.itemView.context).load(newsList[position].urlToImage)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

            }).into(itemNewsHolderBinding.newsImage)

        itemNewsHolderBinding.datePublished.text = getTTimesAgo(newsList[position].publishedAt)
        itemNewsHolderBinding.description.text = newsList[position].description
        itemNewsHolderBinding.newsSource.text = newsList[position].source.name
    }

    override fun getItemCount(): Int {
        return newsList.count()
    }

    inner class NewsViewHolder(binding: ItemNewsHolderBinding) :
        RecyclerView.ViewHolder(binding.root),
        View.OnClickListener {

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val position = absoluteAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                val dataMap: HashMap<String, String> = HashMap()
                dataMap[AppConst.image] = newsList[position].urlToImage
                dataMap[AppConst.title] = newsList[position].title
                dataMap[AppConst.description] = newsList[position].description
                dataMap[AppConst.time] = newsList[position].publishedAt
                dataMap[AppConst.source] = newsList[position].source.name
                dataMap[AppConst.url] = newsList[position].url

                dataIntent(itemView.context, dataMap)
            }
        }
    }
}

