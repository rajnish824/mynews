package com.squareboat.mynews.ui.dashboard

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.squareboat.mynews.R
import com.squareboat.mynews.base.BaseActivity
import com.squareboat.mynews.databinding.ActivityMainBinding
import com.squareboat.mynews.ui.category.CategoryBottomSheet
import com.squareboat.mynews.ui.search.SearchActivity
import com.squareboat.mynews.utils.AppConst
import com.squareboat.mynews.utils.extensions.hide
import com.squareboat.mynews.utils.extensions.show
import com.squareboat.mynews.utils.getCountryNames
import com.squareboat.mynews.viewModel.NewsViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class MainActivity : BaseActivity(),
    CategoryBottomSheet.CategorySheetListener {

    private val newsViewModel: NewsViewModel by viewModel()
    private lateinit var newsAdapter: NewsAdapter

    private var currentPageSize = AppConst.initialPageSize
    private var currentCategory = ""

    private var currentCountry = "in"


    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        getHeadlines(currentCountry, "", currentPageSize)
        initRecyclerView()
        initSearch()
        initPagination()
        setCountryName()

        binding.loadMore.hide()
        binding.filter.shrink()
        binding.loadMore.shrink()
        binding.filter.setOnClickListener {
            val categorySheet = CategoryBottomSheet()
            categorySheet.onItemCategoryListener(this)
            categorySheet.show(supportFragmentManager, "CategorySheet")
        }
        binding.loadMore.setOnClickListener {
            loadMore()
        }
    }

    private fun initRecyclerView() {
        val layoutManager = LinearLayoutManager(this)
        binding.recyclerView.layoutManager = layoutManager
    }

    private fun getHeadlines(
        country: String,
        category: String,
        pageSize: Int
    ) {
        binding.mainProgress.show()
        newsViewModel.allHeadline.observe(this, { headlineList ->
            headlineList?.let {
                if (headlineList.response == null) {
                    headlineList.exception?.let { it1 -> showError(it1) }
                } else {
                    newsAdapter = NewsAdapter(headlineList.response)
                    binding.recyclerView.adapter = newsAdapter
                    newsAdapter.notifyItemInserted(pageSize)
                    binding.recyclerView.smoothScrollToPosition(pageSize - AppConst.incrementSize)
                }
                binding.mainProgress.hide()
            }
        })
        newsViewModel.getHeadlines(
            country = country,
            category = category,
            pageSize = pageSize
        )
        currentCountry = country
        currentCategory = category
        binding.heading.text = getString(R.string.top_headlines)
    }

    private fun initPagination() {
        binding.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (recyclerView.canScrollVertically(1)) {
                    binding.loadMore.hide()
                } else {
                    binding.loadMore.show()
                }
            }
        })
    }

    private fun loadMore() {
        currentPageSize += AppConst.incrementSize
        getHeadlines(
            country = currentCountry,
            category = currentCategory,
            pageSize = currentPageSize
        )
    }

    private fun resetPageSize() {
        currentPageSize = AppConst.initialPageSize
    }

    private fun setCountryName() {
        var countryName = getCountryNames().filterKeys { it == currentCountry }.values.toString()
        countryName = countryName.substring(1, countryName.length - 1)
        binding.location.text = countryName
    }

    private fun initSearch() {
        binding.searchNews.setOnClickListener {
            val intent = Intent(this, SearchActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onCategoryClick(position: Int) {
        resetPageSize()
        getHeadlines(currentCountry, AppConst.category[position], currentPageSize)
    }
}