package com.squareboat.mynews

import androidx.multidex.MultiDexApplication
import com.squareboat.mynews.di.module.appModule
import com.squareboat.mynews.di.module.networkModule
import com.squareboat.mynews.di.module.repositoryModule
import com.squareboat.mynews.di.module.viewModelModule
import org.koin.android.ext.android.startKoin

class MyNEWS : MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()
        initKoin()
    }

    private fun initKoin() {
        startKoin(
            this@MyNEWS,
            listOf(networkModule, appModule, repositoryModule, viewModelModule),
            loadPropertiesFromFile = true
        )
    }
}